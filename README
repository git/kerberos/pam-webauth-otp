                           pam-webauth-otp 1.0
          (OTP PAM module for WebAuth user information service)

                Written by Russ Allbery <eagle@eyrie.org>

  Copyright 2013 The Board of Trustees of the Leland Stanford Junior
  University.  This software is distributed under a BSD-style license.
  See the LICENSE file, included in this distribution, for copyright and
  redistribution information.

BLURB

  The WebAuth web authentication system defines a user information service
  protocol that's used to determine what factors a given user has access
  to and to validate OTP codes used for authentication to the central
  WebLogin server.  This PAM module uses the same protocol to talk to the
  same WebAuth user information service when validating OTP codes for
  interactive PAM-based logins.  Currently, SMS is not supported, only OTP
  methods that don't require a multi-step user interaction.

DESCRIPTION

  WebAuth is a site-wide web authentication system that uses a central
  login server.  That login server supports multifactor authentication
  (and other features) via a user information service provided by the
  local site.  OTP-based multifactor authentication is validated by
  WebAuth via calls to the validate function in that user information
  service.

  pam-webauth-otp is a PAM module that performs the same API calls as the
  WebAuth login server, allowing the same infrastructure and OTP database
  to be used to secure authentications that use PAM.  The user is prompted
  for an OTP code, which is then validated by a call to the validate
  function of a WebAuth user information service.  Any middleware that
  speaks the WebAuth user information service protocol can be used,
  whether or not it is also used for a WebAuth deployment.

  This module currently only supports OTP mechanisms where the user can
  supply a code without any further interaction.  SMS, which requires a
  call to send the SMS message before prompting the user, is not currently
  supported.

REQUIREMENTS

  The PAM implementations on Linux, Solaris, Mac OS X, HP-UX, and AIX
  should theoretically work, although the module is only tested on Linux.
  Use on platforms with other PAM implementations, such as IRIX or the
  *BSDs, will require more porting and will not currently work.  Patches
  are welcome.

  The module is written in C and should hopefully build on any system with
  an adequate PAM library that Libtool supports.

  pam-webauth-otp requires libwebauth from WebAuth 4.5 or later.  WebAuth
  must be built with remctl support.

  pam-webauth-otp can optionally read configuration from krb5.conf as well
  as PAM options; for this, Kerberos libraries are also required.  This
  functionality should work with either MIT Kerberos or Heimdal, but will
  probably not work on Mac OS X since the krb5_appdefault* APIs are
  crippled on that platform.

  Running the complete test suite requires the remctld program be
  installed in /usr/sbin, /usr/local/sbin, or a directory on the builder's
  PATH.  In addition, Perl 5.6 or later plus the following Perl modules
  are required for some tests:

      Test::More (included with Perl 5.8 and later)
      Test::Pod
      Test::Spelling

  All are available on CPAN.  Those tests will be skipped if the modules
  are not available.

  To enable tests that may be sensitive to the local environment or that
  produce a lot of false positives without uncovering many problems, set
  RRA_MAINTAINER_TESTS to a true value.

COMPILING AND INSTALLING

  To build the module, just run:

      ./configure
      make

  Kerberos support is enabled by default if configure can find Kerberos
  libraries.  If your Kerberos libraries aren't installed in a location
  found by your compiler by default and krb5-config isn't on your PATH,
  use the --with-krb5=PATH option to configure.  The Kerberos libraries
  will then be expected in PATH/lib and the headers in PATH/include.

  To specify a particular krb5-config script to use, either set the
  KRB5_CONFIG environment variable or pass it to configure like:

      ./configure KRB5_CONFIG=/path/to/krb5-config

  To not use krb5-config and force library probing even if there is a
  krb5-config script on your path, set KRB5_CONFIG to a nonexistent path:

      ./configure KRB5_CONFIG=/nonexistent

  To disable Kerberos support, even if you have Kerberos libraries
  available, pass --without-krb5 to configure.

  To install the module into /usr/local/lib/security and the man page into
  /usr/local/share/man/man5, run:

      make install

  You can change the installation locations with the --prefix, --mandir,
  and --libdir options to configure.  The module will normally be
  installed in a subdirectory of $libdir named security, but if --prefix
  is set to /usr (rather than /usr/local, the default), the module will be
  installed in /lib/security (or /lib32/security or /lib64/security if
  they exist) to match the default PAM configuration.  Alternately, you
  can simply copy pam_webauth_otp.so to whatever directory you use for PAM
  modules.  On Solaris, you will need to make the module executable.

TESTING

  pam-webauth-otp comes with a test suite, which you can run after
  building pam-afs-session with:

      make check

  If a test case fails, please run the that individual test case with
  verbose output using:

      tests/runtests -o <name-of-test>

  and send me the output when reporting the problem.

CONFIGURING

  Just installing the module does not enable it or change anything about
  your system authentication configuration.  You have to add the module to
  your PAM configuration in the auth group, and you will also need to
  provide the minimum amount of configuration for it to contact the
  WebAuth user information service.  You will at least need to specify
  the server on which that service runs and the command prefix to use.

  For Linux, this will mean a line like:

      auth required pam_webauth_otp.so server=userinfo.example.com \
          command=webkdc

  in the relevant PAM configuration file (all on one line), replacing the
  server parameter with your WebAuth information server host and the
  command with the remctl command that you use for user information
  service calls.

  To affect every login on the system, Debian uses /etc/pam.d/common-auth
  and Red Hat uses /etc/pam.d/system-auth.  Other distributions may use
  other files.  You can also, obviously, use more complex configurations
  if you want OTP to be optional or requested only in combination with
  other forms of authentication.

  For Solaris, add:

      login auth required /usr/local/lib/security/pam_webauth_otp.so \
          server=userinfo.example.com command=webkdc

  to /etc/pam.conf to affect most services, replacing the server and
  command parameters as above.  As with Linux, you may want a different
  configuration to use the module with specific services other than login
  (ssh for instance).

  On all platforms, options can be put after the module name in the PAM
  configuration file.  This is useful if you don't have Kerberos libraries
  available or if you want different configurations for different
  services.

  If Kerberos support was enabled, configuration options may also be put
  in the krb5.conf file used by your Kerberos libraries (usually
  /etc/krb5.conf or /usr/local/etc/krb5.conf) instead or in addition to
  the PAM configuration.  See the man page for more details.  This is
  recommended for general system configuration, since the krb5.conf
  configuration syntax is a little nicer and more flexible.

  If you are using this module with ssh, be aware that it requires
  ChallengeResponseAuthentication (keyboard-interactive).  It will not
  work with the simpler PasswordAuthentication (password) protocol because
  it is prompting for an OTP code, not a password, and doesn't reuse the
  same password as other PAM modules.  Thankfully, most ssh clients now
  support keyboard-interactive.

SUPPORT

  The pam-webauth-otp web page at:

      http://www.eyrie.org/~eagle/software/pam-webauth-otp/

  will always have the current version of this package, the current
  documentation, and pointers to any additional resources.

  New releases are announced via the low-volume webauth-announce mailing
  list.  To subscribe or see the list archives, go to:

      https://mailman.stanford.edu/mailman/listinfo/webauth-announce

  There is also a separate mailing list for general discussion and
  requests for help with WebAuth, and this PAM module can be discussed
  there.  To subscribe or see the list archives, go to:

      https://mailman.stanford.edu/mailman/listinfo/webauth-info

  For Stanford affiliates, this PAM module is a supported product of the
  ITS Infrastructure Delivery Group.  You can report problems or request
  help with WebAuth by submitting a HelpSU ticket at:

      <https://helpsu.stanford.edu/helpsu/3.0/helpsu>

  For non-Stanford users, please instead subscribe to webauth-info and ask
  your question there.  We cannot provide any formal support for
  non-Stanford users, nor do we make any promises or committments related
  to this software.  Please feel free to use it on an as-is basis, and
  please do feel free to send us any patches or improvements that you wish
  to contribute and we will evaluate them for future releases.  However,
  please be aware that our primary focus is supporting the needs of
  Stanford University and work on features not used by Stanford is mostly
  done on a volunteer basis.

SOURCE REPOSITORY

  pam-webauth-otp is maintained using Git.  You can access the current
  source by cloning the repository at:

      git://git.eyrie.org/kerberos/pam-webauth-otp.git

  or view the repository via the web at:

      http://git.eyrie.org/?p=kerberos/pam-webauth-otp.git

  When contributing modifications, patches (possibly generated by
  git-format-patch) are preferred to Git pull requests.

LICENSE

  The pam-webauth-otp package as a whole covered by the following
  copyright statement and license:

    Copyright 2013
        The Board of Trustees of the Leland Stanford Junior University

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  All individual files without an explicit exception below are released
  under this license.  Some files may have additional copyright holders as
  noted in those files.  There is detailed information about the licensing
  of each file in the LICENSE file in this distribution.

  Some files in this distribution are individually released under
  different licenses, all of which are compatible with the above general
  package license but which may require preservation of additional
  notices.  All required notices are preserved in the LICENSE file.
