/*
 * Authentication tests for the pam-webauth-otp module.
 *
 * This test case tests basic authentication success and failure, with
 * end-to-end testing through to the remctl backend.
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Copyright 2013
 *     The Board of Trustees of the Leland Stanford Junior University
 *
 * See LICENSE for licensing terms.
 */

#include <config.h>
#include <portable/pam.h>
#include <portable/system.h>

#include <tests/fakepam/pam.h>
#include <tests/fakepam/script.h>
#include <tests/tap/basic.h>
#include <tests/tap/kerberos.h>
#include <tests/tap/remctl.h>

/* We need stubs for the unimplemented PAM interfaces so fakepam will link. */
#define PAM_STUB(name)                                                  \
    int                                                                 \
    name(pam_handle_t *pamh UNUSED, int flags UNUSED, int argc UNUSED,  \
         const char **argv UNUSED)                                      \
    {                                                                   \
        return PAM_IGNORE;                                              \
    }
PAM_STUB(pam_sm_acct_mgmt)
PAM_STUB(pam_sm_chauthtok)
PAM_STUB(pam_sm_open_session)
PAM_STUB(pam_sm_close_session)


int
main(void)
{
    struct script_config config;
    struct kerberos_config *krbconf;
    const char *source;

    /* Authenticate from the test Kerberos keytab and get the principal. */
    krbconf = kerberos_setup(TAP_KRB_NEEDS_KEYTAB);
    memset(&config, 0, sizeof(config));
    config.extra[0] = krbconf->principal;
    config.extra[1] = krbconf->keytab;

    /*
     * Change directories to the tests directory of the source tree so that
     * remctld will run the correct commands based on the test configuration.
     */
    source = getenv("SOURCE");
    if (source != NULL)
        if (chdir(source) < 0)
            sysbail("cannot chdir to %s", source);

    /* Start remctld. */
    remctld_start(krbconf, "data/conf-webkdc", NULL);

    plan_lazy();

    /* Test for the success case. */
    config.user = "success";
    config.password = "123456";
    run_script("data/scripts/basic/success", &config);

    /* Test for failure due to an invalid code. */
    config.password = "654321";
    run_script("data/scripts/basic/failure", &config);

    /* Test for failure with an invalid user. */
    config.user = "failure";
    config.password = "123456";
    run_script("data/scripts/basic/failure", &config);

    return 0;
}
