# Automake build rules for pam-webauth-otp.
#
# Written by Russ Allbery <eagle@eyrie.org>
# Copyright 2013
#     The Board of Trustees of the Leland Stanford Junior University
#
# See LICENSE for licensing terms.

ACLOCAL_AMFLAGS = -I m4
EXTRA_DIST = .gitignore autogen docs/pam_webauth_otp.pod		  \
	module/pam_webauth_otp.map module/pam_webauth_otp.sym tests/HOWTO \
	tests/TESTS tests/data/cmd-webkdc tests/data/conf-webkdc	  \
	tests/data/krb5.conf tests/data/perl.conf tests/data/scripts	  \
	tests/docs/pod-spelling-t tests/docs/pod-t tests/fakepam/README	  \
	tests/tap/perl/Test/RRA/Automake.pm				  \
	tests/tap/perl/Test/RRA/Config.pm tests/tap/perl/Test/RRA.pm

noinst_LTLIBRARIES = pam-util/libpamutil.la portable/libportable.la
portable_libportable_la_SOURCES = portable/dummy.c portable/krb5.h	\
	portable/macros.h portable/pam.h portable/stdbool.h		\
	portable/system.h
portable_libportable_la_LIBADD = $(LTLIBOBJS)
pam_util_libpamutil_la_SOURCES = pam-util/args.c pam-util/args.h	\
	pam-util/logging.c pam-util/logging.h pam-util/options.c	\
	pam-util/options.h pam-util/vector.c pam-util/vector.h

if HAVE_LD_VERSION_SCRIPT
    VERSION_LDFLAGS = -Wl,--version-script=${srcdir}/module/pam_webauth_otp.map
else
    VERSION_LDFLAGS = -export-symbols ${srcdir}/module/pam_webauth_otp.sym
endif

pamdir = $(libdir)/security
pam_LTLIBRARIES = module/pam_webauth_otp.la
module_pam_webauth_otp_la_SOURCES = module/internal.h module/options.c \
	module/public.c
module_pam_webauth_otp_la_LDFLAGS = -module -shared -avoid-version \
	$(VERSION_LDFLAGS) $(KRB5_LDFLAGS) $(AM_LDFLAGS)
module_pam_webauth_otp_la_LIBADD = pam-util/libpamutil.la \
	portable/libportable.la -lwebauth $(KRB5_LIBS)
dist_man_MANS = docs/pam_webauth_otp.5

# Clean up the backup of config.h.in on make distclean.
DISTCLEANFILES = config.h.in~

# Work around a misfeature of the GNU Coding Standards and really clean all
# the build machinery.
MAINTAINERCLEANFILES = Makefile.in aclocal.m4 build-aux/compile		\
	build-aux/config.guess build-aux/config.sub build-aux/depcomp	\
	build-aux/install-sh build-aux/ltmain.sh build-aux/missing	\
	config.h.in configure docs/pam_webauth_otp.5 m4/libtool.m4	\
	m4/ltoptions.m4 m4/ltsugar.m4 m4/ltversion.m4 m4/lt~obsolete.m4

# Purge the Autotools cache on make distclean.
distclean-local:
	rm -rf autom4te.cache

# A set of flags for warnings.  Add -O because gcc won't find some warnings
# without optimization turned on.  Desirable warnings that can't be turned
# on due to other problems:
#
#     -Wconversion      http://bugs.debian.org/488884 (htons warnings)
#
# Last checked against gcc 4.7.2 (2013-04-22).  -D_FORTIFY_SOURCE=2 enables
# warn_unused_result attribute markings on glibc functions on Linux, which
# catches a few more issues.
WARNINGS = -g -O -D_FORTIFY_SOURCE=2 -Wall -Wextra -Wendif-labels	   \
	-Wformat=2 -Winit-self -Wswitch-enum -Wuninitialized -Wfloat-equal \
	-Wdeclaration-after-statement -Wshadow -Wpointer-arith		   \
	-Wbad-function-cast -Wcast-align -Wwrite-strings		   \
	-Wjump-misses-init -Wlogical-op -Wstrict-prototypes		   \
	-Wold-style-definition -Wmissing-prototypes -Wnormalized=nfc	   \
	-Wpacked -Wredundant-decls -Wnested-externs -Winline -Wvla -Werror

warnings:
	$(MAKE) V=0 CFLAGS='$(WARNINGS)'
	$(MAKE) V=0 CFLAGS='$(WARNINGS)' $(check_PROGRAMS)

# The bits below are for the test suite, not for the main package.
check_PROGRAMS = tests/runtests tests/module/basic-t tests/pam-util/args-t \
	tests/pam-util/fakepam-t tests/pam-util/logging-t		   \
	tests/pam-util/options-t tests/pam-util/vector-t		   \
	tests/portable/asprintf-t tests/portable/snprintf-t		   \
	tests/portable/strndup-t
tests_runtests_CPPFLAGS = -DSOURCE='"$(abs_top_srcdir)/tests"' \
	-DBUILD='"$(abs_top_builddir)/tests"'
check_LIBRARIES = tests/fakepam/libfakepam.a tests/tap/libtap.a
tests_fakepam_libfakepam_a_SOURCES = tests/fakepam/config.c		  \
	tests/fakepam/data.c tests/fakepam/general.c			  \
	tests/fakepam/internal.h tests/fakepam/logging.c		  \
	tests/fakepam/pam.h tests/fakepam/script.c tests/fakepam/script.h
tests_tap_libtap_a_CPPFLAGS = -DPATH_REMCTLD='"$(PATH_REMCTLD)"' \
	$(KRB5_CPPFLAGS) $(AM_CPPFLAGS)
tests_tap_libtap_a_SOURCES = tests/tap/basic.c tests/tap/basic.h	\
	tests/tap/kerberos.c tests/tap/kerberos.h tests/tap/macros.h	\
	tests/tap/process.c tests/tap/process.h tests/tap/remctl.c	\
	tests/tap/remctl.h tests/tap/string.c tests/tap/string.h

# The list of objects and libraries used for module testing by programs that
# link with the fake PAM library or with both it and the module.
MODULE_OBJECTS = module/options.lo module/public.lo pam-util/libpamutil.la \
	tests/fakepam/libfakepam.a

# The test programs themselves.
tests_module_basic_t_LDADD = $(MODULE_OBJECTS) tests/tap/libtap.a \
	portable/libportable.la -lwebauth $(KRB5_LIBS)
tests_pam_util_args_t_LDADD = pam-util/libpamutil.la	\
	tests/fakepam/libfakepam.a tests/tap/libtap.a	\
	portable/libportable.la $(KRB5_LIBS)
tests_pam_util_fakepam_t_LDADD = tests/fakepam/libfakepam.a \
	tests/tap/libtap.a portable/libportable.la
tests_pam_util_logging_t_LDADD = pam-util/libpamutil.la \
	tests/fakepam/libfakepam.a tests/tap/libtap.a	\
	portable/libportable.la $(KRB5_LIBS)
tests_pam_util_options_t_LDADD = pam-util/libpamutil.la \
	tests/fakepam/libfakepam.a tests/tap/libtap.a	\
	portable/libportable.la $(KRB5_LIBS)
tests_pam_util_vector_t_LDADD = pam-util/libpamutil.la	\
	tests/fakepam/libfakepam.a tests/tap/libtap.a	\
	portable/libportable.la
tests_portable_asprintf_t_SOURCES = tests/portable/asprintf-t.c \
	tests/portable/asprintf.c
tests_portable_asprintf_t_LDADD = tests/tap/libtap.a portable/libportable.la
tests_portable_snprintf_t_SOURCES = tests/portable/snprintf-t.c \
	tests/portable/snprintf.c
tests_portable_snprintf_t_LDADD = tests/tap/libtap.a portable/libportable.la
tests_portable_strndup_t_SOURCES = tests/portable/strndup-t.c \
	tests/portable/strndup.c
tests_portable_strndup_t_LDADD = tests/tap/libtap.a portable/libportable.la

check-local: $(check_PROGRAMS)
	cd tests && ./runtests -l $(abs_top_srcdir)/tests/TESTS

# Used by maintainers to run the test suite under valgrind.
check-valgrind: $(check_PROGRAMS)
	rm -rf $(abs_top_builddir)/tmp-valgrind
	mkdir $(abs_top_builddir)/tmp-valgrind
	valgrind --leak-check=full --trace-children=yes --show-reachable=yes \
	    --trace-children-skip="/bin/sh"				     \
	    --log-file=$(abs_top_builddir)/tmp-valgrind/log.%p		     \
	    --suppressions=tests/data/valgrind.supp			     \
	    tests/runtests $(abs_top_srcdir)/tests/TESTS
