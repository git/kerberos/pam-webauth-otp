/*
 * Option handling for pam-webauth-otp.
 *
 * Parses the PAM command line for options to pam-webauth-otp and fills out an
 * allocated structure with those details.
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Copyright 2013
 *     The Board of Trustees of the Leland Stanford Junior University
 *
 * See LICENSE for licensing terms.
 */

#include <config.h>
#include <portable/system.h>

#include <errno.h>

#include <module/internal.h>
#include <pam-util/args.h>
#include <pam-util/logging.h>
#include <pam-util/options.h>

/* Our option definition. */
#define K(name) (#name), offsetof(struct pam_config, name)
static const struct option options[] = {
    { K(command),   true, STRING (NULL)               },
    { K(host),      true, STRING (NULL)               },
    { K(identity),  true, STRING (NULL)               },
    { K(keytab),    true, STRING ("/etc/krb5.keytab") },
    { K(port),      true, NUMBER (0)                  },
    { K(timeout),   true, NUMBER (30)                 },
};
static const size_t optlen = sizeof(options) / sizeof(options[0]);


/*
 * Allocate a new struct pam_args and initialize its data members, including
 * parsing the arguments and getting settings from krb5.conf.
 */
struct pam_args *
pamwao_init(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
    struct pam_args *args;

    args = putil_args_new(pamh, flags);
    if (args == NULL)
        return NULL;
    args->config = calloc(1, sizeof(struct pam_config));
    if (args->config == NULL) {
        putil_crit(args, "cannot allocate memory: %s", strerror(errno));
        putil_args_free(args);
        return NULL;
    }
    if (!putil_args_defaults(args, options, optlen)) {
        free(args->config);
        putil_args_free(args);
        return NULL;
    }
    if (!putil_args_krb5(args, "pam-webauth-otp", options, optlen))
        goto fail;
    if (!putil_args_parse(args, argc, argv, options, optlen))
        goto fail;
    return args;

fail:
    pamwao_free(args);
    return NULL;
}


/*
 * Free the allocated args struct and any memory it points to.
 */
void
pamwao_free(struct pam_args *args)
{
    if (args == NULL)
        return;
    if (args->config != NULL) {
        free(args->config->command);
        free(args->config->host);
        free(args->config->keytab);
        free(args->config);
        args->config = NULL;
    }
    putil_args_free(args);
}
