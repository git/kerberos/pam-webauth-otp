/*
 * Internal prototypes and structures for pam-webauth-otp.
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Copyright 2013
 *     The Board of Trustees of the Leland Stanford Junior University
 *
 * See LICENSE for licensing terms.
 */

#ifndef INTERNAL_H
#define INTERNAL_H 1

#include <config.h>
#include <portable/pam.h>
#include <portable/macros.h>

/* Forward declarations to avoid unnecessary includes. */
struct pam_args;

/* Used for unused parameters to silence gcc warnings. */
#define UNUSED  __attribute__((__unused__))

/*
 * The global structure holding our arguments from the PAM configuration.
 * Filled in by pamafs_init.
 */
struct pam_config {
    char *host;                 /* Host for user information queries. */
    long port;                  /* Port for user information queries. */
    char *identity;             /* Identity for user information queries. */
    char *command;              /* Base command for queries. */
    char *keytab;               /* Keytab for authentication. */
    long timeout;               /* Timeout for authentication call. */
};

BEGIN_DECLS

/* Default to a hidden visibility for all internal functions. */
#pragma GCC visibility push(hidden)

/* Parse the PAM flags and arguments and fill out pam_args. */
struct pam_args *pamwao_init(pam_handle_t *, int flags, int argc,
                             const char **argv);

/* Free the pam_args struct when we're done. */
void pamwao_free(struct pam_args *);

/* Undo default visibility change. */
#pragma GCC visibility pop

END_DECLS

#endif /* INTERNAL_H */
