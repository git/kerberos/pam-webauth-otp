/*
 * The public APIs of the pam-webauth-otp PAM module.
 *
 * Provides the public pam_sm_authenticate function, plus whatever other stubs
 * we need to satisfy PAM.
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Copyright 2013
 *     The Board of Trustees of the Leland Stanford Junior University
 *
 * See LICENSE for licensing terms.
 */

#include <config.h>
#include <portable/pam.h>
#include <portable/system.h>

#include <errno.h>
#include <webauth/basic.h>
#include <webauth/factors.h>
#include <webauth/webkdc.h>

#include <module/internal.h>
#include <pam-util/args.h>
#include <pam-util/logging.h>


/*
 * Use PAM conversation functions to obtain the OTP code from the user.
 * Returns a PAM status code.
 */
static int
get_code(struct pam_args *args, char **code)
{
    int pamret;
    struct pam_conv *conv;
    struct pam_message *msg;
    struct pam_response *resp = NULL;

    /* Obtain the conversation function from the application. */
    pamret = pam_get_item(args->pamh, PAM_CONV, (PAM_CONST void **) &conv);
    if (pamret != 0) {
        putil_err_pam(args, pamret, "cannot get conversation function");
        return PAM_SERVICE_ERR;
    }
    if (conv->conv == NULL) {
        putil_err(args, "no PAM conversation function");
        return PAM_SERVICE_ERR;
    }

    /* Allocate memory for the question and response. */
    msg = calloc(1, sizeof(struct pam_message));
    if (msg == NULL) {
        putil_crit(args, "cannot allocate memory: %s", strerror(errno));
        return PAM_SERVICE_ERR;
    }
    msg->msg = "Enter code: ";
    msg->msg_style = PAM_PROMPT_ECHO_ON;

    /* Call into the application conversation function. */
    pamret = conv->conv(1, (PAM_CONST struct pam_message **) &msg,
                        &resp, conv->appdata_ptr);
    if (pamret != PAM_SUCCESS)
        goto cleanup;
    if (resp == NULL || resp->resp == NULL) {
        pamret = PAM_SERVICE_ERR;
        goto cleanup;
    }
    *code = strdup(resp->resp);
    if (*code == NULL) {
        putil_crit(args, "cannot allocate memory: %s", strerror(errno));
        pamret = PAM_SERVICE_ERR;
        goto cleanup;
    }
    pamret = PAM_SUCCESS;

cleanup:
    free(msg);
    if (resp != NULL) {
        free(resp->resp);
        free(resp);
    }
    return pamret;
}


/*
 * Perform an authentication.
 *
 * We currently prompt the user for an OTP token and pass it to the validation
 * function with no further complexity.  Eventually, more of this behavior
 * will need to be configurable.
 */
int
pam_sm_authenticate(pam_handle_t *pamh, int flags, int argc,
                    const char *argv[])
{
    struct pam_args *args;
    int s;
    int pamret = PAM_SERVICE_ERR;
    struct webauth_context *ctx = NULL;
    struct webauth_user_config config;
    struct webauth_user_validate *result;
    char *code = NULL;
    const char *message;
    PAM_CONST char *user;

    /* Workaround for WebAuth bug. */
    char *state = NULL;

    args = pamwao_init(pamh, flags, argc, argv);
    if (args == NULL)
        goto done;
    ENTRY(args, flags);

    /* Initialize and configure WebAuth. */
    s = webauth_context_init(&ctx, NULL);
    if (s != WA_ERR_NONE) {
        putil_crit(args, "cannot initialize WebAuth context: %s",
                   webauth_error_message(ctx, s));
        goto done;
    }
    memset(&config, 0, sizeof(config));
    config.protocol = WA_PROTOCOL_REMCTL;
    config.host     = args->config->host;
    config.port     = args->config->port;
    config.identity = args->config->identity;
    config.command  = args->config->command;
    config.keytab   = args->config->keytab;
    config.timeout  = args->config->timeout;
    s = webauth_user_config(ctx, &config);
    if (s != WA_ERR_NONE) {
        putil_err(args, "cannot configure user information service: %s",
                   webauth_error_message(ctx, s));
        goto done;
    }

    /* Get the username from PAM. */
    pamret = pam_get_user(args->pamh, &user, NULL);
    if (pamret != PAM_SUCCESS || user == NULL) {
        if (pamret == PAM_CONV_AGAIN)
            pamret = PAM_INCOMPLETE;
        else
            pamret = PAM_SERVICE_ERR;
        goto done;
    }
    args->user = user;

    /* Prompt for and validate the OTP code. */
    pamret = get_code(args, &code);
    if (pamret != PAM_SUCCESS)
        goto done;
    memset(&result, 0, sizeof(result));
    s = webauth_user_validate(ctx, user, NULL, code, NULL, state, &result);
    if (s != WA_ERR_NONE) {
        putil_err(args, "cannot call OTP validation service: %s",
                  webauth_error_message(ctx, s));
        pamret = PAM_SERVICE_ERR;
        goto done;
    }
    if (!result->success) {
        message = result->user_message;
        if (message == NULL)
            message = "OTP validation failed";
        putil_log_failure(args, "%s", message);
        pamret = PAM_AUTH_ERR;
        goto done;
    }
    pam_syslog(args->pamh, LOG_INFO,
               "user %s authenticated with OTP method %s", user,
               webauth_factors_string(ctx, result->factors));
    pamret = PAM_SUCCESS;

done:
    EXIT(args, pamret);
    if (ctx != NULL)
        webauth_context_free(ctx);
    pamwao_free(args);
    return pamret;
}


/*
 * All PAM modules in the auth group must provide a setcred interface, but we
 * don't do anything.  Just silently return success.
 */
int 
pam_sm_setcred(pam_handle_t *pamh UNUSED, int flags UNUSED, int argc UNUSED,
               const char *argv[] UNUSED)
{
    return PAM_SUCCESS;
}
